<?php

namespace App\Http\Controllers\Backend\Access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\Paper;
use App\Syllabus;
use App\Book;
use App\Subject;
use App\Question;
use App\PaperQuestion;
use App\PaperSubject;
use Response;
use Illuminate\Support\Facades\DB;

class PapersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $papers = Paper::orderBy('id','desc')->get();
        return view("backend.papers.index",  compact("papers"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books =    Book::where('status', 'active')->orderBy('id')->get();
        $subjects = Subject::where('status', 'active')->orderBy('id')->get();
        $listSubjects = array();
        $listBooks = array('' => 'Please select');

        if(!empty($books))
        {
            foreach($books as $book)
            {
                $listBooks[$book->id] = $book->name;
            }
        }

        if(!empty($subjects))
        {
            foreach($subjects as $subject)
            {
                $listSubjects[$subject->id] = $subject->name;
            }
        }

        return view("backend.papers.create",  compact('listBooks','listSubjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'book_id' => 'required',
            'name' => 'required',
            'to' => 'required|array|min:1',
            'subjects' => 'required|array|min:1',
        ],[
            'to.required'=>'The question field is required.',
            'subjects.required'=>'The Subject field is required.',
            'name.required'=>'The paper name field is required.',
            ]);
        
        $request->offsetSet('user_id', access()->user()->id);
        
         if(!$request->has('status'))
         {
             $request->offsetSet('status', 'inactive');
         }
         
         $paperDataArray = array(
            'book_id' => $request->get('book_id'),
            'name' => $request->get('name'),
//            'total_time' => $request->get('hours').":".$request->get('minutes').":00",
//            'mark_awardable' => $request->get('mark_awardable'),
            'user_id' => access()->user()->id,
            'status' => $request->get('status'),
         );
        
        $paper = new Paper($paperDataArray);
        if($paper->save())
        {
            $paperQues = array();
            $paperSubs = array();
            foreach($request->get('to') as $ques)
            {
                $paperQues[] = New PaperQuestion(['question_id' => $ques]);
                
            }
            foreach ($request->get('subjects') as $sub) {
                $paperSubs[] = New PaperSubject(['subject_id' => $sub]);
            }
            $paper->paperQuestions()->saveMany($paperQues);
            $paper->paperSubjects()->saveMany($paperSubs);
        }
        
        return redirect()->route('admin.access.papers.index')->withFlashSuccess("Successfully Created");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paper = Paper::find($id);
        $books =    Book::where('status', 'active')->orderBy('id')->get();
        $subjects = Subject::where('status', 'active')->orderBy('id')->get();
        $listSubjects = array();
        $listBooks = array('' => 'Please select');

        if(!empty($books))
        {
            foreach($books as $book)
            {
                $listBooks[$book->id] = $book->name;
            }
        }

        if(!empty($subjects))
        {
            foreach($subjects as $subject)
            {
                $listSubjects[$subject->id] = $subject->name;
            }
        }


        return view("backend.papers.edit",  compact('paper','listSubjects','listBooks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'to' => 'required|array|min:1',
        ],[
            'to.required'=>'The question field is required.',
            'name.required'=>'The paper name field is required.',
        ]);

        $request->offsetSet('user_id', access()->user()->id);

        if(!$request->has('status'))
        {
            $request->offsetSet('status', 'inactive');
        }

        $paperDataArray = array(
            'name' => $request->get('name'),
            'user_id' => access()->user()->id,
            'status' => $request->get('status'),
        );

        $paper = Paper::find($id);

        if($paper->update($paperDataArray))
        {
            $paper->paperQuestions()->delete();
            $paperQues = array();
            foreach($request->get('to') as $ques)
            {
                $paperQues[] = New PaperQuestion(['question_id' => $ques]);

            }
            $paper->paperQuestions()->saveMany($paperQues);
        }
        return redirect()->route('admin.access.papers.index')->withFlashSuccess("Successfully Updated");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $paper = Paper::find($id);
       $paper->delete();
        return redirect()->route('admin.access.papers.index')->withFlashSuccess("Successfully Deleted");
    }
}
