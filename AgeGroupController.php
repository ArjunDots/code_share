<?php

namespace App\Http\Controllers\Backend\Access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\AgeGroup;

class AgeGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ageGroup = AgeGroup::orderBy('id','desc')->get();
        return view("backend.ageGroup.index",  compact("ageGroup"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.ageGroup.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
           'age'=>'Required' 
        ]);
        
        $request->offsetSet('user_id', access()->user()->id);
        
        if(!$request->has('status'))
        {
            $request->offsetSet('status', 'inactive');
        }       
        
        $ageGroup = $request->all();
        
        AgeGroup::create($ageGroup);
        return redirect()->route('admin.access.ageGroup.index')->withFlashSuccess("Successfully Created");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ageGroup = AgeGroup::find($id);
        return view("backend.ageGroup.edit", compact('ageGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'age'=>'Required' 
        ]);
         
         $ageGroup = AgeGroup::find($id);
         /**/
         if(!$request->has('status'))
         {
               $request->offsetSet('status', 'inactive');
         }
         /**/
         $ageGroupUpdate = $request->all();
         //dd($bookUpdate);
         $ageGroup->update($ageGroupUpdate);
         return redirect()->route('admin.access.ageGroup.index')->withFlashSuccess("Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ageGroup = AgeGroup::find($id);
        $ageGroup->delete();
        return redirect()->route('admin.access.ageGroup.index')->withFlashSuccess("Successfully Deleted");
    }
}
