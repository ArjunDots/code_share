<?php

namespace App\Http\Controllers\Backend\Access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\Book;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('id','desc')->get();
        return view("backend.books.index",  compact("books"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.books.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name'=>'Required' 
        ]);
        
        $request->offsetSet('user_id', access()->user()->id);
        /**/
          if(!$request->has('status'))
         {
             $request->offsetSet('status', 'inactive');
         }
        /**/
        
        $book = $request->all();
        
        Book::create($book);
        return redirect()->route('admin.access.books.index')->withFlashSuccess("Successfully Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        return view("backend.books.edit", compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
           'name'=>'Required' 
        ]);
         
         $book = Book::find($id);
         /**/
         if(!$request->has('status'))
         {
               $request->offsetSet('status', 'inactive');
         }
         /**/
         $bookUpdate = $request->all();
         //dd($bookUpdate);
         $book->update($bookUpdate);
         return redirect()->route('admin.access.books.index')->withFlashSuccess("Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect()->route('admin.access.books.index')->withFlashSuccess("Successfully Deleted");
    }
}
